package com.rike.shouye;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/shouye")
public class ShouyeController {

    @RequestMapping("/shouye")
    public String getShouye(){
        return "首页";
    }
}
